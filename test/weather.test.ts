import weather from "../src/Weather";
import axios, { AxiosResponse } from "axios";
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const response = {
  coord: { lon: 1.33, lat: 45.75 },
  weather: [
    {
      id: 802,
      main: "Clouds",
      description: "scattered clouds",
      icon: "03n"
    }
  ],
  base: "stations",
  main: {
    temp: 11.17,
    feels_like: 6.27,
    temp_min: 10.56,
    temp_max: 12.22,
    pressure: 1014,
    humidity: 37
  },
  visibility: 10000,
  wind: { speed: 3.6, deg: 330 },
  clouds: { all: 26 },
  dt: 1585851021,
  sys: {
    type: 1,
    id: 6459,
    country: "FR",
    sunrise: 1585805528,
    sunset: 1585851829
  },
  timezone: 7200,
  id: 2998285,
  name: "Arrondissement de Limoges",
  cod: 200
};

const city = {
  parameters: {
    fields: {
      location: {
        structValue: { fields: { city: { stringValue: "Limoges" } } }
      }
    }
  }
};

const emptyCity = {
  parameters: {
    fields: {

    }
  }
};

const axios200Response: AxiosResponse<any> = {
  data: response,
  status: 200,
  statusText: "OK",
  config: {},
  headers: {}
};

const axios500Response: AxiosResponse<any> = {
  data: null,
  status: 503,
  statusText: "An internal server occured",
  config: {},
  headers: {}
};

describe("test weather", () => {
  it("should repond", done => {
    mockedAxios.get.mockImplementationOnce(() => Promise.resolve(axios200Response));
    weather(city, "appId").then(res => {
      expect(res.displayText).toEqual("11.17°, scattered clouds à Limoges");
      done();
    });
  });

  it("should repond badly if api do not find the city", done => {
    mockedAxios.get.mockImplementationOnce(() => Promise.reject({ response: { data: { message: "J'ai pas trouvé" } } }));
    weather(city, "appId").then(res => {
      expect(res.displayText).toEqual("J'ai pas trouvé Limoges");
      done();
    });
  });

  it("Handle API error", done => {
    mockedAxios.get.mockImplementationOnce(() => Promise.reject(axios500Response));
    weather(city, "appId").then(res => {
      expect(res.displayText).toEqual("Erreur lors de la récupération des infos.... Limoges");
      done();
    });
  });

  it("Respond if dialog flow did not find the city", done => {
    mockedAxios.get.mockImplementationOnce(() => Promise.reject(axios500Response));
    weather(emptyCity, "appId").then(res => {
      expect(res.displayText).toEqual("Ville non reconnue");
      done();
    });
  });
});
