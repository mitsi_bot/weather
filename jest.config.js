module.exports = {
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  moduleFileExtensions: ["js", "ts", "json"],
  testMatch: ["**/test/**/*.test.(ts|js)"],
  testEnvironment: "node",
  testPathIgnorePatterns: ["node_modules", "dist"],
  "collectCoverageFrom": [
    "**/src/**/*.{js,ts}",
    "!**/node_modules/**",
    "!**/src/types/**",
    "!**/dist/**",
  ]
};
