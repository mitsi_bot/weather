export interface IWeatherResponse {
  coord: { lon: number, lat: number },
  weather: IWeather[],
  base: string,
  main: IWeatherMain,
  visibility: number,
  wind: { speed: number, deg: number },
  clouds: { all: number },
  dt: number,
  sys: {
    type: number,
    id: number,
    country: string,
    sunrise: number,
    sunset: number
  },
  timezone: number,
  id: number,
  name: string,
  cod: number
}

export interface IWeatherMain {
  temp: number,
  feels_like: number,
  temp_min: number,
  temp_max: number,
  pressure: number,
  humidity: number
}

export interface IWeather {
  id: number,
  main: string,
  description: string,
  icon: string
}
