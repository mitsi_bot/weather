import { IWeather, IWeatherResponse, IWeatherMain } from './WeatherResponse';
import axios, { AxiosResponse } from "axios";
import { path, pathOr } from "ramda";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";

export default function weather(serviceInput: ServiceInput, appId:string): Promise<ResponseText> {
  return new Promise((resolve, _reject) => {
    const city = path(
      [
        "parameters",
        "fields",
        "location",
        "structValue",
        "fields",
        "city",
        "stringValue"
      ],
      serviceInput
    ) as string;
    if (!city) {
      resolve(new ResponseText("Ville non reconnue"));
    }
    const url = `https://api.openweathermap.org/data/2.5/weather?units=metric&lang=fr&q=${encodeURI(city)}&appid=${appId}`;
    axios
      .get(url)
      .then((response: AxiosResponse<IWeatherResponse>) => {
        const main = pathOr({}, ["data", "main"], response) as IWeatherMain;
        const weather = pathOr([{}], ["data", "weather"], response)[0] as IWeather;
        const result = `${main.temp}°, ${weather.description} à ${city}`;
        resolve(new ResponseText(result));
      })
      .catch((err: Error) => {
        console.error(err)
        let result = pathOr("Erreur lors de la récupération des infos....", ["response", "data", "message"], err) as string;
        result += ` ${city}`;
        resolve(new ResponseText(result));
      });
  });
}