import express, { Response, Request } from "express";
const config = require('config-yml');
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import ServiceInput from "mitsi_bot_lib/dist/src/ServiceInput";
import Weather from "./Weather";

class Server extends BaseService {
    onPost(request: Request, response: Response): void {
        const startDate = new Date();
        Weather(request.body as ServiceInput, config.app.appId)
            .then((res: ResponseText) => {
                response.json(res);
                console.debug("TTR:", new Date().getTime() - startDate.getTime())
            }).catch((err: Error) => {
                console.error(err);
                response.json(new ResponseText("Weather error"));
                console.debug("TTR:", new Date().getTime() - startDate.getTime())
            });
    }
}

new Server(express()).startServer();